Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: auto-07p
Source: https://sourceforge.net/projects/auto-07p/

Files: *
Copyright: 1979-2007, E.~J.~Doedel, California Institute of
 Technology, and Concordia University.  All rights reserved.
License: BSD-3-clause

Files: ./07p/python/parseB.py
       ./07p/python/parseBandS.py
       ./07p/python/parseC.py
       ./07p/python/parseH.py
       ./07p/python/parseS.py
Copyright: 1997, Randy Paffenroth and John Maddocks
License: GPL-2+

Files: ./07p/src/lapack.f
Copyright: 1992-2011 The University of Tennessee and The University
           of Tennessee Research Foundation,
           2000-2011 The University of California Berkeley,
           2006-2011 The University of Colorado Denver
License: BSD-3-clause

Files: ./07p/doc/epstopdf.pl
Copyright: 2011, Karl Berry
License: BSD-3-clause

Files: ./07p/python/AUTOutil.py
Copyright: 1991-1995, Stichting Mathematisch Centrum,
           Amsterdam, The Netherlands
License: MIT/X11
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted,
 provided that the above copyright notice appear in all copies and that
 both that copyright notice and this permission notice appear in
 supporting documentation, and that the names of Stichting Mathematisch
 Centrum or CWI or Corporation for National Research Initiatives or
 CNRI not be used in advertising or publicity pertaining to
 distribution of the software without specific, written prior
 permission.
 .
 While CWI is the initial source for this software, a modified version
 is made available by the Corporation for National Research Initiatives
 (CNRI) at the Internet address ftp://ftp.python.org.
 .
 STICHTING MATHEMATISCH CENTRUM AND CNRI DISCLAIM ALL WARRANTIES WITH
 REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL STICHTING MATHEMATISCH
 CENTRUM OR CNRI BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
 DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.

Files: debian/*
Copyright: 2009-2012, Sergey B Kirpichev <skirpichev@gmail.com>
License: GPL-2+

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.i
 - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer listed
   in this license in the documentation and/or other materials
   provided with the distribution.
 - Neither the name of the copyright holders nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2, or (at your option) any
 later version.
Comment:
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License (GPL) version 2 can be found at
 /usr/share/common-licenses/GPL-2.
